import { Test, TestingModule } from '@nestjs/testing';
import { IFORMATTER_TOKEN, IPARSER_TOKEN } from '../../interfaces';
import { AuctionBidDto, AuctionDto, AuctionWinnerDto } from '../dto';
import { AuctionFormatterService, AuctionParserService, AuctionService } from '../services';
import { AuctionController } from './auction.controller';

describe('AppController', () => {
  let auctionController: AuctionController;

  beforeEach(async () => {
    const app: TestingModule = await Test.createTestingModule({
      controllers: [AuctionController],
      providers: [
        AuctionService,
        {
          provide: IPARSER_TOKEN,
          useClass: AuctionParserService,
        },
        {
          provide: IFORMATTER_TOKEN,
          useClass: AuctionFormatterService,
        },],
    }).compile();

    auctionController = app.get<AuctionController>(AuctionController);
  });

  describe('AuctionController', () => {

    it('should return bidder E as auction winner at price of 130 (> reservePrice = 100) ', async () => {
      const auctionDto = new AuctionDto();
      auctionDto.bids = [
        new AuctionBidDto('A', [110, 130]),
        new AuctionBidDto('B', []),
        new AuctionBidDto('C', [125]),
        new AuctionBidDto('D', [105, 115, 90]),
        new AuctionBidDto('E', [132, 135, 140]),
      ];
      auctionDto.reservePrice = 100;
      expect(await auctionController.findBidWinner(auctionDto)).toStrictEqual(new AuctionWinnerDto('E', 130));
    });

    it('should return bidder E as auction winner at price of 135 (== reservePrice) ', async () => {
      const auctionDto = new AuctionDto();
      auctionDto.bids = [
        new AuctionBidDto('A', [110, 130]),
        new AuctionBidDto('B', []),
        new AuctionBidDto('C', [125]),
        new AuctionBidDto('D', [105, 115, 90]),
        new AuctionBidDto('E', [132, 135, 140]),
      ];
      auctionDto.reservePrice = 135;
      expect(await auctionController.findBidWinner(auctionDto)).toStrictEqual(new AuctionWinnerDto('E', 135));
    });
  });
});
