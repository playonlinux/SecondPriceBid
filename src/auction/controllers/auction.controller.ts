import {
  Body,
  Controller,
  HttpCode,
  Inject,
  Post,
  UsePipes,
  ValidationPipe
} from '@nestjs/common';
import { ApiBadRequestResponse, ApiOkResponse, ApiTags } from '@nestjs/swagger';
import {
  IFORMATTER_TOKEN,
  IFormatter,
  IPARSER_TOKEN,
  IParser,
} from '../../interfaces';
import { AuctionDto, AuctionWinnerDto } from '../dto';
import { Auction } from '../models';
import { AuctionWinner } from '../models/auction-winner.model';
import { AuctionService } from '../services';

@ApiTags('Auction')
@Controller('auction')
export class AuctionController {
  constructor(
    @Inject(IPARSER_TOKEN)
    private readonly auctionParser: IParser<AuctionDto, Auction>,
    private readonly auctionService: AuctionService,
    @Inject(IFORMATTER_TOKEN)
    private readonly auctionFormatter: IFormatter<
      AuctionWinner,
      AuctionWinnerDto
    >
  ) { }

  @Post('bids/winner')
  @HttpCode(200)
  @ApiOkResponse({
    type: AuctionWinnerDto,
    description: 'The auction winner and the final price.',
  })
  @ApiBadRequestResponse({ description: 'Bad request.' })
  @UsePipes(new ValidationPipe({ forbidUnknownValues: true }))
  async findBidWinner(@Body() auctionDto: AuctionDto) {
    const auction: Auction = await this.auctionParser.parse(auctionDto);
    const winner: AuctionWinner = this.auctionService.findBidWinner(
      auction.bids,
      auction.reservePrice,
    );
    return this.auctionFormatter.format(winner);

  }
}
