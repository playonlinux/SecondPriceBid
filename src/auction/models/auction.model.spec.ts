import { validate } from 'class-validator';
import { AuctionBid } from './auction-bid.model';
import { Auction } from './auction.model';

describe('Auction', () => {

  it('should throw when bids array and reservePrice fields are both missing', async () => {
    const auction = new Auction(undefined, undefined);
    const errors = await validate(auction);
    expect(errors.length).not.toBe(0);
  });

  it('should throw when bids array field is missing', async () => {
    const auction = new Auction(undefined, undefined);
    auction.reservePrice = 100;
    const errors = await validate(auction);
    expect(errors.length).not.toBe(0);
  });
  it('should throw when bids array field is empty array', async () => {
    const auction = new Auction(undefined, undefined);
    auction.bids = [];
    auction.reservePrice = 100;
    const errors = await validate(auction);
    expect(errors.length).not.toBe(0);
  });


  it('should throw when reservePrice field is missing', async () => {
    const auction = new Auction(undefined, undefined);
    auction.bids = [new AuctionBid('A', [200])];
    const errors = await validate(auction);
    expect(errors.length).not.toBe(0);
  });

  it('should not throw when bids and reservePrice field are filled', async () => {
    const auction = new Auction(undefined, undefined);
    auction.bids = [new AuctionBid('A', [200])];
    auction.reservePrice = 100;
    const errors = await validate(auction);
    expect(errors.length).toBe(0);
  });
});

