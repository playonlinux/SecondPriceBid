import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsNumber, IsString } from 'class-validator';
import { Model } from '../../interfaces';

export class AuctionWinner extends Model {
  @ApiProperty()
  @IsString()
  @IsNotEmpty()
  bidder: string;

  @ApiProperty()
  @IsNumber()
  @IsNotEmpty()
  price: number;

  constructor(_bidder: string, _price: number) {
    super();
    this.bidder = _bidder;
    this.price = _price;
  }
}
