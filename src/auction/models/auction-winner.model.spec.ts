import { validate } from 'class-validator';
import { AuctionWinner } from './auction-winner.model';

describe('AuctionWinner', () => {

  it('should throw when bidder and prices fields are both missing', async () => {
    const auctionWinner = new AuctionWinner(undefined, undefined);
    const errors = await validate(auctionWinner);
    expect(errors.length).not.toBe(0);
  });

  it('should throw when bidder field is missing', async () => {
    const auctionWinner = new AuctionWinner(undefined, 20);
    const errors = await validate(auctionWinner);
    expect(errors.length).not.toBe(0);
  });

  it('should throw when price field is missing', async () => {
    const auctionWinner = new AuctionWinner('A', undefined);
    const errors = await validate(auctionWinner);
    expect(errors.length).not.toBe(0);
  });

  it('should not throw when bidder and price field are filled', async () => {
    const auctionWinner = new AuctionWinner('A', 200);
    const errors = await validate(auctionWinner);
    expect(errors.length).toBe(0);
  });
});

