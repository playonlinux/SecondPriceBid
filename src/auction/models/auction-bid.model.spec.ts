import { validate } from 'class-validator';
import { AuctionBid } from './auction-bid.model';

describe('AuctionBid', () => {

  it('should throw when bidder and prices array fields are both missing', async () => {
    const auctionBid = new AuctionBid(undefined, undefined);
    const errors = await validate(auctionBid);
    expect(errors.length).not.toBe(0);
  });

  it('should throw when bidder field is missing', async () => {
    const auctionBid = new AuctionBid(undefined, []);
    const errors = await validate(auctionBid);
    expect(errors.length).not.toBe(0);
  });

  it('should throw when prices field is missing', async () => {
    const auctionBid = new AuctionBid('A', undefined);
    const errors = await validate(auctionBid);
    expect(errors.length).not.toBe(0);
  });

  it('should not throw when bidder and prices field are filled', async () => {
    const auctionBid = new AuctionBid('A', [200]);
    const errors = await validate(auctionBid);
    expect(errors.length).toBe(0);
  });
});

