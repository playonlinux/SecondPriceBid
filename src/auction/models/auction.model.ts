import { ApiProperty } from '@nestjs/swagger';
import { ArrayNotEmpty, IsArray, IsNotEmpty, IsNumber } from 'class-validator';
import { Model } from '../../interfaces';
import { AuctionBid } from './auction-bid.model';

export class Auction extends Model {
  @ApiProperty({ type: [AuctionBid] })
  @IsArray()
  @ArrayNotEmpty()
  @IsNotEmpty()
  bids: AuctionBid[];

  @ApiProperty()
  @IsNumber()
  @IsNotEmpty()
  reservePrice: number;

  constructor(_bids: AuctionBid[], _reservePrice: number) {
    super();
    this.bids = _bids;
    this.reservePrice = _reservePrice;
  }
}
