import { ApiProperty } from '@nestjs/swagger';
import { IsArray, IsNotEmpty, IsString } from 'class-validator';
import { Model } from '../../interfaces';

export class AuctionBid extends Model {
  @ApiProperty()
  @IsString()
  @IsNotEmpty()
  bidder: string;

  @ApiProperty({ type: [Number] })
  @IsArray()
  @IsNotEmpty()
  prices: number[];

  constructor(_bidder: string, _prices: number[]) {
    super();
    this.bidder = _bidder;
    this.prices = _prices;
  }
}
