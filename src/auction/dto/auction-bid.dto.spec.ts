import { validate } from 'class-validator';
import { AuctionBidDto } from './auction-bid.dto';

describe('AuctionBidDto', () => {

  it('should throw when bidder and prices array fields are both missing', async () => {
    const auctionBidDto = new AuctionBidDto(undefined, undefined);
    const errors = await validate(auctionBidDto);
    expect(errors.length).not.toBe(0);
  });

  it('should throw when bidder field is missing', async () => {
    const auctionBidDto = new AuctionBidDto(undefined, []);
    const errors = await validate(auctionBidDto);
    expect(errors.length).not.toBe(0);
  });

  it('should throw when prices field is missing', async () => {
    const auctionBidDto = new AuctionBidDto('A', undefined);
    const errors = await validate(auctionBidDto);
    expect(errors.length).not.toBe(0);
  });

  it('should not throw when bidder and prices field are filled', async () => {
    const auctionBidDto = new AuctionBidDto('A', [200]);
    const errors = await validate(auctionBidDto);
    expect(errors.length).toBe(0);
  });
});

