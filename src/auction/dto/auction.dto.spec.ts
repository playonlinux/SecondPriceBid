import { validate } from 'class-validator';
import { AuctionBidDto, AuctionDto } from '.';

describe('AuctionDto', () => {

  it('should throw when bids array and reservePrice fields are both missing', async () => {
    const auctionDto = new AuctionDto();
    const errors = await validate(auctionDto);
    expect(errors.length).not.toBe(0);
  });

  it('should throw when bids array field is missing', async () => {
    const auctionDto = new AuctionDto();
    auctionDto.reservePrice = 100;
    const errors = await validate(auctionDto);
    expect(errors.length).not.toBe(0);
  });

  it('should throw when reservePrice field is missing', async () => {
    const auctionDto = new AuctionDto();
    auctionDto.bids = [];
    const errors = await validate(auctionDto);
    expect(errors.length).not.toBe(0);
  });

  it('should not throw when bids and reservePrice field are filled', async () => {
    const auctionDto = new AuctionDto();
    auctionDto.bids = [new AuctionBidDto('A', [200])];
    auctionDto.reservePrice = 100;
    const errors = await validate(auctionDto);
    expect(errors.length).toBe(0);
  });
});

