import { validate } from 'class-validator';
import { AuctionWinnerDto } from './auction-winner.dto';

describe('AuctionWinnerDto', () => {

  it('should throw when bidder and prices fields are both missing', async () => {
    const auctionWinnerDto = new AuctionWinnerDto(undefined, undefined);
    const errors = await validate(auctionWinnerDto);
    expect(errors.length).not.toBe(0);
  });

  it('should throw when bidder field is missing', async () => {
    const auctionWinnerDto = new AuctionWinnerDto(undefined, 20);
    const errors = await validate(auctionWinnerDto);
    expect(errors.length).not.toBe(0);
  });

  it('should throw when price field is missing', async () => {
    const auctionWinnerDto = new AuctionWinnerDto('A', undefined);
    const errors = await validate(auctionWinnerDto);
    expect(errors.length).not.toBe(0);
  });

  it('should not throw when bidder and price field are filled', async () => {
    const auctionWinnerDto = new AuctionWinnerDto('A', 200);
    const errors = await validate(auctionWinnerDto);
    expect(errors.length).toBe(0);
  });
});

