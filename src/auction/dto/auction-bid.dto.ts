import { ApiProperty } from '@nestjs/swagger';
import { IsArray, IsNotEmpty, IsString } from 'class-validator';
import { DTO } from '../../interfaces';

export class AuctionBidDto extends DTO {
  @ApiProperty({ required: true })
  @IsString()
  @IsNotEmpty()
  bidder: string;
  
  @ApiProperty({ default: [], required: true, type: [Number] })
  @IsArray()
  @IsNotEmpty()
  prices: number[];

  constructor(_bidder: string, _prices: number[]) {
    super();
    this.bidder = _bidder;
    this.prices = _prices;
  }
}
