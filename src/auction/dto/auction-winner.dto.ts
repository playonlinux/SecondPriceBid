import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsNumber, IsString } from 'class-validator';
import { DTO } from '../../interfaces';

export class AuctionWinnerDto extends DTO {
  @ApiProperty({ required: true })
  @IsString()
  @IsNotEmpty()
  bidder: string;

  @ApiProperty({ required: true })
  @IsNumber()
  @IsNotEmpty()
  price: number;

  constructor(_bidder: string, _price: number) {
    super();
    this.bidder = _bidder;
    this.price = _price;
  }
}
