import { ApiProperty } from '@nestjs/swagger';
import { ArrayNotEmpty, IsArray, IsNotEmpty, IsNumber } from 'class-validator';
import { DTO } from '../../interfaces';
import { AuctionBidDto } from './auction-bid.dto';

export class AuctionDto extends DTO {
  @ApiProperty({ required: true, type: [AuctionBidDto] })
  @IsArray()
  @ArrayNotEmpty()
  @IsNotEmpty()
  bids: AuctionBidDto[];

  @ApiProperty({required: true })
  @IsNumber()
  @IsNotEmpty()
  reservePrice: number;
}
