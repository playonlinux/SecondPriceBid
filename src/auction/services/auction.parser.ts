import { Injectable } from '@nestjs/common';
import { IParser } from '../../interfaces';
import { AuctionBidDto, AuctionDto } from '../dto';
import { Auction, AuctionBid } from '../models';

@Injectable()
export class AuctionParserService implements IParser<AuctionDto, Auction> {
  async parse(auctionDto: AuctionDto): Promise<Auction> {
    const auctionBids = auctionDto.bids.map(
      (auctionBidDto: AuctionBidDto) =>
        new AuctionBid(auctionBidDto.bidder, auctionBidDto.prices),
    );
    return new Auction(auctionBids, auctionDto.reservePrice)
  }
}
