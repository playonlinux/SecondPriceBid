import { Injectable } from '@nestjs/common';
import { IFormatter } from 'src/interfaces';
import { AuctionWinnerDto } from '../dto/auction-winner.dto';
import { AuctionWinner } from '../models/auction-winner.model';

@Injectable()
export class AuctionFormatterService
  implements IFormatter<AuctionWinner, AuctionWinnerDto>
{
  format(auctionWinner: AuctionWinner): AuctionWinnerDto {
    return new AuctionWinnerDto(auctionWinner.bidder, auctionWinner.price);
  }
}
