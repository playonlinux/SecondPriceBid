import { Injectable } from '@nestjs/common';
import { AuctionBid } from '../models';
import { AuctionWinner } from '../models/auction-winner.model';

@Injectable()
export class AuctionService {
  findBidWinner(bids: AuctionBid[], reservePrice: number): AuctionWinner {
    const sortedBids: AuctionBid[] = bids.sort(
      (a1: AuctionBid, a2: AuctionBid) =>
        Math.max(...a1.prices) > Math.max(...a2.prices) ? -1 : 1,
    );
    return new AuctionWinner(
      sortedBids[0].bidder,
      Math.max(...sortedBids[1].prices, reservePrice),
    );
  }
}
