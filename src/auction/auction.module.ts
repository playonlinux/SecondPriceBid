import { Module } from '@nestjs/common';
import { IFORMATTER_TOKEN, IPARSER_TOKEN } from '../interfaces';
import { AuctionController } from './controllers';
import {
  AuctionFormatterService,
  AuctionParserService,
  AuctionService,
} from './services';

@Module({
  imports: [],
  controllers: [AuctionController],
  providers: [
    AuctionService,
    {
      provide: IPARSER_TOKEN,
      useClass: AuctionParserService,
    },
    {
      provide: IFORMATTER_TOKEN,
      useClass: AuctionFormatterService,
    },
  ],
})
export class AuctionModule {}
