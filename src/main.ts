import { NestFactory } from '@nestjs/core';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import { AuctionModule } from './auction/auction.module';

async function bootstrap() {
  const app = await NestFactory.create(AuctionModule);

  const config = new DocumentBuilder()
    .setTitle('Auction')
    .setDescription('Sealed-bid second-price auction')
    .setVersion('1.0')
    .addTag('Auction')
    .build();
  const document = SwaggerModule.createDocument(app, config);
  SwaggerModule.setup('api', app, document);
  await app.listen(3000);
}
bootstrap();
