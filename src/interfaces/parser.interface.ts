import { DTO } from './dto.interface';
import { Model } from './model.interface';

export interface IParser<T extends DTO, U extends Model> {
  parse(input: T): Promise<U>;
}

export const IPARSER_TOKEN = Symbol('IParser');
