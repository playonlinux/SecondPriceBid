import { INestApplication } from '@nestjs/common';
import { Test, TestingModule } from '@nestjs/testing';
import * as request from 'supertest';
import { AuctionModule } from '../src/auction/auction.module';

describe('AppController (e2e)', () => {
  let app: INestApplication;

  beforeEach(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [AuctionModule],
    }).compile();

    app = moduleFixture.createNestApplication();
    await app.init();
  });

  describe('Auction test suite', () => {
    it('should return BadRequest when bids array and reservePrice fields are both missing', () => {
      return request(app.getHttpServer())
        .post('/auction/bids/winner')
        .expect(400);
    });
    it('should return BadRequest when bids array field is missing', () => {
      return request(app.getHttpServer())
        .post('/auction/bids/winner')
        .send({ reservePrice: 0 })
        .expect(400);
    });
    it('should return BadRequest when reservePrice field is missing', () => {
      return request(app.getHttpServer())
        .post('/auction/bids/winner')
        .send({ bids: [] })
        .expect(400);
    });
    it('should return BadRequest when bids field is not an array', () => {
      return request(app.getHttpServer())
        .post('/auction/bids/winner')
        .send({ bids: 'bids', reservePrice: 0 })
        .expect(400);
    });
    it('should return BadRequest when reservePrice field is not an number', () => {
      return request(app.getHttpServer())
        .post('/auction/bids/winner')
        .send({ bids: [], reservePrice: 'hello' })
        .expect(400);
    });
    it('should return bidder E as auction winner at price of 130 (> reservePrice = 100) ', () => {
      return request(app.getHttpServer())
        .post('/auction/bids/winner')
        .send({
          bids: [
            {
              bidder: 'A',
              prices: [110, 130],
            },
            {
              bidder: 'B',
              prices: [],
            },
            {
              bidder: 'C',
              prices: [125],
            },
            {
              bidder: 'D',
              prices: [105, 115, 90],
            },
            {
              bidder: 'E',
              prices: [132, 135, 140],
            },
          ],
          reservePrice: 100,
        })
       // .expect(200)
        .expect({ bidder: 'E', price: 130 });
    });
    it('should return bidder E as auction winner at price of 135 (== reservePrice) ', () => {
      return request(app.getHttpServer())
        .post('/auction/bids/winner')
        .send({
          bids: [
            {
              bidder: 'A',
              prices: [110, 130],
            },
            {
              bidder: 'B',
              prices: [],
            },
            {
              bidder: 'C',
              prices: [125],
            },
            {
              bidder: 'D',
              prices: [105, 115, 90],
            },
            {
              bidder: 'E',
              prices: [132, 135, 140],
            },
          ],
          reservePrice: 135,
        })
       // .expect(200)
        .expect({ bidder: 'E', price: 135 });
    });
  });
});
