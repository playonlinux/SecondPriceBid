'use strict';

customElements.define('compodoc-menu', class extends HTMLElement {
    constructor() {
        super();
        this.isNormalMode = this.getAttribute('mode') === 'normal';
    }

    connectedCallback() {
        this.render(this.isNormalMode);
    }

    render(isNormalMode) {
        let tp = lithtml.html(`
        <nav>
            <ul class="list">
                <li class="title">
                    <a href="index.html" data-type="index-link">second-price-bid documentation</a>
                </li>

                <li class="divider"></li>
                ${ isNormalMode ? `<div id="book-search-input" role="search"><input type="text" placeholder="Type to search"></div>` : '' }
                <li class="chapter">
                    <a data-type="chapter-link" href="index.html"><span class="icon ion-ios-home"></span>Getting started</a>
                    <ul class="links">
                        <li class="link">
                            <a href="overview.html" data-type="chapter-link">
                                <span class="icon ion-ios-keypad"></span>Overview
                            </a>
                        </li>
                        <li class="link">
                            <a href="index.html" data-type="chapter-link">
                                <span class="icon ion-ios-paper"></span>README
                            </a>
                        </li>
                                <li class="link">
                                    <a href="dependencies.html" data-type="chapter-link">
                                        <span class="icon ion-ios-list"></span>Dependencies
                                    </a>
                                </li>
                                <li class="link">
                                    <a href="properties.html" data-type="chapter-link">
                                        <span class="icon ion-ios-apps"></span>Properties
                                    </a>
                                </li>
                    </ul>
                </li>
                    <li class="chapter modules">
                        <a data-type="chapter-link" href="modules.html">
                            <div class="menu-toggler linked" data-bs-toggle="collapse" ${ isNormalMode ?
                                'data-bs-target="#modules-links"' : 'data-bs-target="#xs-modules-links"' }>
                                <span class="icon ion-ios-archive"></span>
                                <span class="link-name">Modules</span>
                                <span class="icon ion-ios-arrow-down"></span>
                            </div>
                        </a>
                        <ul class="links collapse " ${ isNormalMode ? 'id="modules-links"' : 'id="xs-modules-links"' }>
                            <li class="link">
                                <a href="modules/AuctionModule.html" data-type="entity-link" >AuctionModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-bs-toggle="collapse" ${ isNormalMode ?
                                            'data-bs-target="#controllers-links-module-AuctionModule-1b462ad661b913c2a0a3740d13bc39b384e4595710101a99c15993d96e2c468f09009b8d665b9791a200bb5f46839360653ca0528d31239e0729142b0161d583"' : 'data-bs-target="#xs-controllers-links-module-AuctionModule-1b462ad661b913c2a0a3740d13bc39b384e4595710101a99c15993d96e2c468f09009b8d665b9791a200bb5f46839360653ca0528d31239e0729142b0161d583"' }>
                                            <span class="icon ion-md-swap"></span>
                                            <span>Controllers</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="controllers-links-module-AuctionModule-1b462ad661b913c2a0a3740d13bc39b384e4595710101a99c15993d96e2c468f09009b8d665b9791a200bb5f46839360653ca0528d31239e0729142b0161d583"' :
                                            'id="xs-controllers-links-module-AuctionModule-1b462ad661b913c2a0a3740d13bc39b384e4595710101a99c15993d96e2c468f09009b8d665b9791a200bb5f46839360653ca0528d31239e0729142b0161d583"' }>
                                            <li class="link">
                                                <a href="controllers/AuctionController.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >AuctionController</a>
                                            </li>
                                        </ul>
                                    </li>
                                <li class="chapter inner">
                                    <div class="simple menu-toggler" data-bs-toggle="collapse" ${ isNormalMode ?
                                        'data-bs-target="#injectables-links-module-AuctionModule-1b462ad661b913c2a0a3740d13bc39b384e4595710101a99c15993d96e2c468f09009b8d665b9791a200bb5f46839360653ca0528d31239e0729142b0161d583"' : 'data-bs-target="#xs-injectables-links-module-AuctionModule-1b462ad661b913c2a0a3740d13bc39b384e4595710101a99c15993d96e2c468f09009b8d665b9791a200bb5f46839360653ca0528d31239e0729142b0161d583"' }>
                                        <span class="icon ion-md-arrow-round-down"></span>
                                        <span>Injectables</span>
                                        <span class="icon ion-ios-arrow-down"></span>
                                    </div>
                                    <ul class="links collapse" ${ isNormalMode ? 'id="injectables-links-module-AuctionModule-1b462ad661b913c2a0a3740d13bc39b384e4595710101a99c15993d96e2c468f09009b8d665b9791a200bb5f46839360653ca0528d31239e0729142b0161d583"' :
                                        'id="xs-injectables-links-module-AuctionModule-1b462ad661b913c2a0a3740d13bc39b384e4595710101a99c15993d96e2c468f09009b8d665b9791a200bb5f46839360653ca0528d31239e0729142b0161d583"' }>
                                        <li class="link">
                                            <a href="injectables/AuctionService.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >AuctionService</a>
                                        </li>
                                    </ul>
                                </li>
                            </li>
                </ul>
                </li>
                        <li class="chapter">
                            <div class="simple menu-toggler" data-bs-toggle="collapse" ${ isNormalMode ? 'data-bs-target="#controllers-links"' :
                                'data-bs-target="#xs-controllers-links"' }>
                                <span class="icon ion-md-swap"></span>
                                <span>Controllers</span>
                                <span class="icon ion-ios-arrow-down"></span>
                            </div>
                            <ul class="links collapse " ${ isNormalMode ? 'id="controllers-links"' : 'id="xs-controllers-links"' }>
                                <li class="link">
                                    <a href="controllers/AuctionController.html" data-type="entity-link" >AuctionController</a>
                                </li>
                            </ul>
                        </li>
                    <li class="chapter">
                        <div class="simple menu-toggler" data-bs-toggle="collapse" ${ isNormalMode ? 'data-bs-target="#classes-links"' :
                            'data-bs-target="#xs-classes-links"' }>
                            <span class="icon ion-ios-paper"></span>
                            <span>Classes</span>
                            <span class="icon ion-ios-arrow-down"></span>
                        </div>
                        <ul class="links collapse " ${ isNormalMode ? 'id="classes-links"' : 'id="xs-classes-links"' }>
                            <li class="link">
                                <a href="classes/Auction.html" data-type="entity-link" >Auction</a>
                            </li>
                            <li class="link">
                                <a href="classes/AuctionBid.html" data-type="entity-link" >AuctionBid</a>
                            </li>
                            <li class="link">
                                <a href="classes/AuctionBidDto.html" data-type="entity-link" >AuctionBidDto</a>
                            </li>
                            <li class="link">
                                <a href="classes/AuctionDto.html" data-type="entity-link" >AuctionDto</a>
                            </li>
                            <li class="link">
                                <a href="classes/AuctionWinner.html" data-type="entity-link" >AuctionWinner</a>
                            </li>
                            <li class="link">
                                <a href="classes/AuctionWinnerDto.html" data-type="entity-link" >AuctionWinnerDto</a>
                            </li>
                            <li class="link">
                                <a href="classes/DTO.html" data-type="entity-link" >DTO</a>
                            </li>
                            <li class="link">
                                <a href="classes/Model.html" data-type="entity-link" >Model</a>
                            </li>
                        </ul>
                    </li>
                        <li class="chapter">
                            <div class="simple menu-toggler" data-bs-toggle="collapse" ${ isNormalMode ? 'data-bs-target="#injectables-links"' :
                                'data-bs-target="#xs-injectables-links"' }>
                                <span class="icon ion-md-arrow-round-down"></span>
                                <span>Injectables</span>
                                <span class="icon ion-ios-arrow-down"></span>
                            </div>
                            <ul class="links collapse " ${ isNormalMode ? 'id="injectables-links"' : 'id="xs-injectables-links"' }>
                                <li class="link">
                                    <a href="injectables/AuctionFormatterService.html" data-type="entity-link" >AuctionFormatterService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/AuctionParserService.html" data-type="entity-link" >AuctionParserService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/AuctionService.html" data-type="entity-link" >AuctionService</a>
                                </li>
                            </ul>
                        </li>
                    <li class="chapter">
                        <div class="simple menu-toggler" data-bs-toggle="collapse" ${ isNormalMode ? 'data-bs-target="#interfaces-links"' :
                            'data-bs-target="#xs-interfaces-links"' }>
                            <span class="icon ion-md-information-circle-outline"></span>
                            <span>Interfaces</span>
                            <span class="icon ion-ios-arrow-down"></span>
                        </div>
                        <ul class="links collapse " ${ isNormalMode ? ' id="interfaces-links"' : 'id="xs-interfaces-links"' }>
                            <li class="link">
                                <a href="interfaces/IFormatter.html" data-type="entity-link" >IFormatter</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/IParser.html" data-type="entity-link" >IParser</a>
                            </li>
                        </ul>
                    </li>
                    <li class="chapter">
                        <div class="simple menu-toggler" data-bs-toggle="collapse" ${ isNormalMode ? 'data-bs-target="#miscellaneous-links"'
                            : 'data-bs-target="#xs-miscellaneous-links"' }>
                            <span class="icon ion-ios-cube"></span>
                            <span>Miscellaneous</span>
                            <span class="icon ion-ios-arrow-down"></span>
                        </div>
                        <ul class="links collapse " ${ isNormalMode ? 'id="miscellaneous-links"' : 'id="xs-miscellaneous-links"' }>
                            <li class="link">
                                <a href="miscellaneous/functions.html" data-type="entity-link">Functions</a>
                            </li>
                            <li class="link">
                                <a href="miscellaneous/variables.html" data-type="entity-link">Variables</a>
                            </li>
                        </ul>
                    </li>
                        <li class="chapter">
                            <a data-type="chapter-link" href="routes.html"><span class="icon ion-ios-git-branch"></span>Routes</a>
                        </li>
                    <li class="chapter">
                        <a data-type="chapter-link" href="coverage.html"><span class="icon ion-ios-stats"></span>Documentation coverage</a>
                    </li>
                    <li class="divider"></li>
                    <li class="copyright">
                        Documentation generated using <a href="https://compodoc.app/" target="_blank" rel="noopener noreferrer">
                            <img data-src="images/compodoc-vectorise.png" class="img-responsive" data-type="compodoc-logo">
                        </a>
                    </li>
            </ul>
        </nav>
        `);
        this.innerHTML = tp.strings;
    }
});